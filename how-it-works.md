# How It Works

Xand is a private settlement network that allows for a fast, non-reversible, bank-agnostic, 24x7 payment solution system that operates within the existing U.S. legal and regulatory frameworks. Whereas traditional payment networks can take up to a week to settle, and can be reversible, Xand instead settles in under 60 seconds, making your funds immediately available for use.

To use Xand, Members deposit funds in a Member bank (and refill the Member bank account as necessary). Members can then use the Xand Network's streamlined Create, Send, and Redeem workflows to transmit Member funds within the Xand network.

* **Create** moves funds from a Member's bank deposit account into the Xand network.
* **Send** transfers funds in the Xand network to other Members.
* **Redeem** moves funds from the Xand network back into a Member's bank deposit account.

When funds are transferred onto the Xand network they are recorded on an immutable ledger (blockchain). Transfers between network Members, as well as redemptions are also recorded as transactions on the blockchain. The immutable nature of the blockchain is what allows for the fast, final nature of Xand transactions. This is all supported by Validators, who secure the blockchain via proof-of-stake consensus method, without the high energy costs of a proof-of-work algorithm. This allows for _trustless_ transactions, where you do not have to know the entities that you're transacting with. Cryptographic assurances guarantee your transactions and safeguard the confidentiality of members and of transactions within the Network.

The Xand system has its own on-chain governance, which allows Members to join and leave the network at will and for Validators, Trustees, and Limited Agents to be changed as required. The result is a dynamic network that will continue to support the needs of its members over time.

* [Xand Technical White Paper](**LINK**) — Information on the technical aspects of the Xand Network.
* [Confidentiality for the Xand Blockchain](**LINK**) — Cryptographic model behind Xand's confidentiality protections.
* [Create/Send/Redeem Workflow](**LINK**) — Implementation details for the Xand API.
