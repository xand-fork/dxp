# Implement: Redeem

After you have claims on the network assigned to your Member address, through your own deposit or through receipt from other members, you may eventually wish to redeem some of them to your bank deposit account, thereby transferring the claims back into fiat currency. This is done with the Redeem workflow, which removes claims from your Member address and then transfers the corresponding amount of fiat currency from the Trustee account to your registered bank deposit account.

This is a one-step process:

* `POST /transactions/claims/redeem` — Transfer claims back into fiat currency at the Member's bank account.

## POST /transactions/claims/redeem
#### Transfer claims back into fiat currency at the Member's bank account.

To redeem claims back your bank deposit account, simply initiate the `POST /transactions/claims/redeem` command for a specific amount of claims to be sent to a specific bank deposit account. This is the mirror to the `POST /transactions/claims/create` command.

Once you have requested a redemption of funds, the Trustee Service automatically manages the redemption, which should be nearly instantaneous, except in cases where a potential third party bank API response may delay the redemption. 

### Example Request

This redeems 9,999 units, which would be $99.99 if the unit is USD, to bank account #1 in the system.

```
{
  "accountId": 1,
  "amountInMinorUnit": 9999
}
```

### Request Fields

#### accountId
[required, integer]

The account receiving the redemption. 
> **Important**: This must be the account associated with the active Member, else funds will become inaccessible.

#### amountInMinorUnit
[required, integer]

The amount of the redemption, where minor unit is cents for USD.

### Example Response

```
{
  "transactionId": "0x1749f72ce95e18c9eae8c28c0a35aae9915effa2168f3bdb3e5312a368bf4ed4",
  "nonce": "0xe2e03b07c98c9489855bd63d5de5a773"
}
```

#### transactionId
[string]

The identifier for the transaction, which can be retrieved with the `GET /transactions` or `GET /transactions/{transactionid}` commands.

#### nonce
[string]

A correlation ID. This is used to link the transaction to a transfer of fiat funds, so you might record it for verification purposes, but the rest of the process will be automated with the approval of the Trustee.