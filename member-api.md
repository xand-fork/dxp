# Member API

The Xand network can be accessed by a Member API, a RESTful API that gives you as a Member the ability to transact within the Xand network. The Rest endpoint for Member API commands is `/api/{version}/` on the machine and port where Xand network software is running. 

The standard REST architecture is used, with resources accessible using the `GET`, `POST`, `PUT`, and `DELETE` HTTP request methods. When additional data must be sent to the Xand network, it's done using the request body of a `POST` or `PUT` command. Each message also includes an `Authorization:` header that contains a Bearer token authenticating the user's identity.

A full message for the simple `/member/address` GET command, accessed through `curl`, is formatted as follows:
```
curl -X GET "http://localhost:3000/api/v1/member/address" -H  "accept: application/json" -H  "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJYQU5EX05FVFdPUktfR0VORVJBVE9SIiwic3ViIjoiREVWRUxPUEVSIiwiZXhwIjo0MTAyMzU4NDAwLCJjb21wYW55IjoiQVdFU09NRUNPIn0.wMe5Ji5CKhK5xcOP9RR0mIYUGoB8HVs6NZbN0PtKeIM"
```
The API endpoints for the Xand Member API are currently divided into five categories:

* `/accounts/`: View participant accounts.
* `/banks/`: View participating banks.
* `/member/`: View current user.
* `/service/`:  Examine overall service.
* `/transactions/`: Create, transfer, or redeem claims.

The heart of the system is in the transactions, which follow the standard Xand workflow: claims are created to fund Members on the Xand network, using bank deposit accounts; claims are transferred among Members on the Xand network to make payments; and claims are redeemed to return funds to bank deposit accounts from the Xand network.

## /accounts/

Accounts are the Xand-network representations of the Members' bank accounts.  Each account is associated with a specific `bank` where the Member holds a deposit account; this is the deposit account used for claim creation and redemption.

* `GET /accounts`: List all accounts.
* `POST /accounts`: Register a new account.
* `DELETE /accounts/{accountId}`: Remove an account's registration.
* `GET /accounts/{accountId}`: Retrieve information for a specific account.
* `PUT /accounts/{accountId}`: Edit an account's information.
* `GET /accounts/{accountId}/balance`: Get the fiat balance of an account.

### GET /accounts

**Purpose:** List all accounts registered for a Xand network.

**Response:** Returns a JSON array of objects, each of which contains information on an account registered on the Xand network.

_Example:_
```
[
  {
    "id": 1,
    "bankId": 1,
    "shortName": "Account 0",
    "bankName": "test-bank-one",
    "maskedAccountNumber": "XXXXXXXX0000",
    "routingNumber": "990000000"
  },
  {
    "id": 2,
    "bankId": 2,
    "shortName": "Account 0",
    "bankName": "test-bank-two",
    "maskedAccountNumber": "XXXXXXXX0000",
    "routingNumber": "991000000"
  }
]
```
Each object contains the following information about the account:

* `id`: The identifier for the account.
* `shortName`: The short, human-readable name of the account.
* `maskedAccountNumber`: The deposit account at the bank associated with this Member.

Each object also contains the following information about the bank associated with the account:

* `bankId`: The identifier of the bank.
* `bankName`: The human-readable name of the bank.
* `routingNumber`: The ABA routing number for the bank.

**Response Codes:**

* `200`: Successful retrieval.
* `401`: Authentication error.
* `500`: Server error.

> **Cryptographic Notes:** Deposit account numbers are masked to maintain confidentiality.

### POST /accounts

**Purpose:** Register a new account on the Xand network.

**Request Body:** Requires a JSON object that names the account and links it appropriately to a bank deposit account.

_Example:_
```
{
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "accountNumber": "1234567890"
}
```

* `shortName`: The short, human-readable name for the account.
* `bankId`: The identifier of the Member's deposit bank.
* `accountNumber`: The number of the Member's deposit account at the bank.

**Response:** Returns a JSON object containing the full description of the account, exactly as would be returned by `GET /accounts`.

_Example:_
```
{
  "id": 3,
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "bankName": "test-bank-one",
  "maskedAccountNumber": "XXXXXXXX7890",
  "routingNumber": "990000000"
}
```

Each object contains the following information about the account:

* `id`: The identifier for the account.
* `shortName`: The short, human-readable name of the account.
* `maskedAccountNumber`: The deposit account at the bank associated with this Member.

Each object also contains the following information about the bank associated with the account:

* `bankId`: The identifier of the bank.
* `bankName`: The human-readable name of the bank.
* `routingNumber`: The ABA routing number for the bank.

**Response Codes:**

* `200`: Successful registration.
* `400`: Input or other client error.
* `401`: Authentication error.
* `404`: Bank account not found.
* `500`: Server error.

> **Cryptographic Notes:** Deposit account numbers are masked to maintain confidentiality.

### DELETE /accounts/{accountId}

**Purpose:** Delete the Xand-network registration of an account.

**Response Codes:**

* `200`: Successful deletion.
* `401`: Authentication error.
* `404`: Account ID not found.
* `500`: Server error.

### GET /accounts/{accountId}

**Purpose:** Retrieve information on a specific account.

**Response:** Returns a JSON object containing the full description of the account, exactly as would be returned by `GET /accounts`.

_Example:_
```
{
  "id": 3,
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "bankName": "test-bank-one",
  "maskedAccountNumber": "XXXXXXXX7890",
  "routingNumber": "990000000"
}
```

Each object contains the following information about the account:

* `id`: The identifier for the account.
* `shortName`: The short, human-readable name of the account.
* `maskedAccountNumber`: The deposit account at the bank associated with this Member.

Each object also contains the following information about the bank associated with the account:

* `bankId`: The identifier of the bank.
* `bankName`: The human-readable name of the bank.
* `routingNumber`: The ABA routing number for the bank.

**Response Codes:**

* `200`: Successful retrieval.
* `401`: Authentication error.
* `404`: Account ID not found.
* `500`: Server error.

> **Cryptographic Notes:** Deposit account numbers are masked to maintain confidentiality.

### PUT /accounts/{accountId}

**Purpose:** Update the name and/or deposit account registration for an account.

**Request Body:** Requires a JSON object containing the `shortName` and `accountNumber` values. Both keys must be included or the command will fail.

_Example:_
```
{
  "shortName": "Discretionary Funds",
  "accountNumber": "1234567890"
}
```
* `shortName`: The short, human-readable name for the account.
* `accountNumber`: The number of the Member's deposit account at the bank.

Note that `bankId` may not be updated for an account.

**Response:** Returns a JSON object containing the full description of the updated account, exactly as would be returned by `GET /accounts`.

_Example:_
```
{
  "id": 3,
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "bankName": "test-bank-one",
  "maskedAccountNumber": "XXXXXXXX7890",
  "routingNumber": "990000000"
}
```

Each object contains the following information about the account:

* `id`: The identifier for the account.
* `shortName`: The short, human-readable name of the account.
* `maskedAccountNumber`: The deposit account at the bank associated with this account.

Each object also contains the following information about the bank associated with the account:

* `bankId`: The identifier of the bank.
* `bankName`: The human-readable name of the bank.
* `routingNumber`: The ABA routing number for the bank.

**Response Codes:**

* `200`: Successful update.
* `400`: Input or other client error.
* `401`: Authentication error.
* `404`: Account ID not found.
* `500`: Server error.

> **Cryptographic Notes:** Deposit account numbers are masked to maintain confidentiality.

### GET /accounts/{accountId}/balance

**Purpose:** Determine the balance of an account.

**Response:** Returns a JSON object listing the fiat monetary balance of a deposit account.

_Example:_
```
{
  "accountId": "1",
  "currentBalanceInMinorUnit": 1000000000,
  "availableBalanceInMinorUnit": 1000000000
}
```

* `accountId`: The account being checked.
* `currentBalanceInMinorUnit`: Full balance in a minor unit such as cents (for USD).
* `availableBalanceInMinorUnit`: Available balance in a minor unit such as cents (for USD).

**Response Codes:**

* `200`: Successful retrieval.
* `401`: Authentication error.
* `404`: Account ID not found.
* `500`: Server error.

## /banks/

Participating banks give members the ability to deposit money in order to create claims on the Xand network and to receive money when they redeem claims on the Xand network. The Trustee has an account at each participating bank for a network, for receiving and distributing funds for the network. A Member will have a deposit account at one of the participating banks, which will be recorded as their `account`. They will interact with the Trustee deposit account at the same bank when creating and redeeming claims.

* `GET /banks`: List all banks.
* `POST /banks`: Register a new bank.
* `DELETE /banks/{bankId}`: Remove a bank's registration.
* `GET /banks/{bankId}`: Retrieve information for a specific bank.
* `POST /banks/{bankId}`: Edit a bank's information.

### GET /banks/

**Purpose:** Retrieve a list of banks registered with a Xand network.

**Response:** Returns a JSON array of JSON objects, each describing a bank.

_Example:_
```
[
  {
    "id": 1,
    "name": "test-bank-one",
    "routingNumber": "<Routing Number>",
    "trustAccount": "<Trust Account Number>",
    "adapter": {
      "<Bank Adapter Name>": {
        "url": "<Bank API URL>",
        "secretApiKeyId": "<API Key Id Secret Key>",
        "secretApiKeyValue": "<API Key Value Secret Key>"
      }
    }
  },
  {
    "id": 2,
    "name": "test-bank-two",
    "routingNumber": "<Routing Number>",
    "trustAccount": "<Trust Account Number>",
    "adapter": {
      "<Bank Adapter Name>": {
        "url": "<Bank API URL>",
        "secretApiKeyId": "<API Key Id Secret Key>",
        "secretApiKeyValue": "<API Key Value Secret Key>"
      }
    }
  }
]
```
Each bank lists the following information:

* `id`: The identifier of the bank.
* `name`: The human-readable name of the bank.
* `routingNumber`: The ABA routing number for the bank.
* `trustAccount`: The deposit account for the Trustee at the bank.
* `adapter`: Information for accessing the bank's API.

**Response Codes:**

* `200`: Successful retrieval.
* `401`: Authentication error.
* `500`: Server error.

> **Financial Notes:** When claims are funded, money is transferred from a Member's deposit account to the Trustee account associated with the Member's bank; when claims are redeemed, money is transferred from the Trustee account to the Member's deposit account.

### POST /banks

**Purpose:** Register a bank with a Xand network.

**Request Body:** Requires a JSON object containing the full information on a bank as would be retrieved by `GET /banks`, except without the `id` (which will be returned) and with only one `adapter`.

_Example:_
```
{
  "name": "<Bank Name>",
  "routingNumber": "<Routing Number>",
  "trustAccount": "<Trust Account Number>",
  "adapter": {
    "<Bank Adapter Name>": {
      "url": "<Bank API Url>",
      "secretUserName": "<Your Username Secret Key>",
      "secretPassword": "<Your Password Secret Key>",
      "secretClientAppIdent": "<Your Client Authentication>",
      "secretOrganizationId": "<Your Organization Id>"
    }
  }
}
```
* `name`: The human-readable name of the bank.
* `routingNumber`: The ABA routing number for the bank.
* `trustAccount`: The deposit account for the Trustee at the bank.
* `adapter`: Information for accessing the bank's API. For a `"<Bank Name>"` adapter, include: `url`, `secretApiKeyId`, and `secretApiKeyValue`. For a `"<Bank Name>"` adapter, include `url`, `secretUserName`, `secretPassword`, `secretClientAppIdent`, and `secretOrganizationId`.

**Response:** Returns a JSON object containing the input bank, plus an `id`, exactly as would be returned by `GET /banks`.

_Example:_
```
{
  "id": <Bank Id>,
  "name": "<Bank Name>",
  "routingNumber": "<Routing Number>",
  "trustAccount": "<Trust Account Number>",
  "adapter": {
    "<Bank Adapter Name>": {
      "url": "<Bank API Url>",
      "secretUserName": "<Your Username Secret Key>",
      "secretPassword": "<Your Password Secret Key>",
      "secretClientAppIdent": "<Your Client Authentication>",
      "secretOrganizationId": "<Your Organization Id>"
    }
  }
}
```

**Response Codes:**

* `200`: Successful registration.
* `401`: Authentication error.
* `404`: Bank ID not found.
* `500`: Server error.

### DELETE /banks/{bankId}

**Purpose:** Remove a bank's registration from a Xand network.

**Response Codes:**

* `200`: Successful deletion.
* `401`: Authentication error.
* `404`: Bank ID not found.
* `500`: Server error.

### GET /banks/{bankId}

**Purpose:** Retrieve information on a specific bank.

**Response:** Displays information on a specific bank, exactly as would be returned by `GET /banks`.
```
{
  "id": <Bank Id>,
  "name": "<Bank Name>",
  "routingNumber": "<Routing Number>",
  "trustAccount": "<Trust Account Number>",
  "adapter": {
    "<Bank Adapter Name>": {
      "url": "<Bank API Url>",
      "secretUserName": "<Your Username Secret Key>",
      "secretPassword": "<Your Password Secret Key>",
      "secretClientAppIdent": "<Your Client Authentication>",
      "secretOrganizationId": "<Your Organization Id>"
    }
  }
}
```
* `id`: The identifier of the bank.
* `name`: The human-readable name of the bank.
* `routingNumber`: The ABA routing number for the bank.
* `trustAccount`: The deposit account for the Trustee at the bank.
* `adapter`: Information for accessing the bank's API. 

**Response Codes:**

* `200`: Successful retrieval.
* `401`: Authentication error.
* `404`: Bank ID not found.
* `500`: Server error.

### PUT /banks/{bankId}

**Purpose:** Update information on a specific bank.

**Request Body:** Requires a JSON object containing the full information on a bank as would be entered through `POST /banks`. Any data may be updated other than the ID.

_Example:_
```
{
  "name": "<Bank Name>",
  "routingNumber": "<Routing Number>",
  "trustAccount": "<Trust Account Number>",
  "adapter": {
    "<Bank Adapter Name>": {
      "url": "<Bank API Url>",
      "secretUserName": "<Your Username Secret Key>",
      "secretPassword": "<Your Password Secret Key>",
      "secretClientAppIdent": "<Your Client Authentication>",
      "secretOrganizationId": "<Your Organization Id>"
    }
  }
}
```
* `name`: The human-readable name of the bank.
* `routingNumber`: The ABA routing number for the bank.
* `trustAccount`: The deposit account for the Trustee at the bank.
* `adapter`: Information for accessing the bank's API. For a `"<Bank Name>"` adapter, include: `url`, `secretApiKeyId`, and `secretApiKeyValue`. For a `"<Bank Name>"` adapter, include `url`, `secretUserName`, `secretPassword`, `secretClientAppIdent`, and `secretOrganizationId`.

**Response:** Returns a JSON object containing the updated bank, plus an `id`, exactly as would be returned by `GET /banks`.

_Example:_
```
{
  "id": <Bank Id>,
  "name": "<Bank Name>",
  "routingNumber": "<Routing Number>",
  "trustAccount": "<Trust Account Number>",
  "adapter": {
    "<Bank Adapter Name>": {
      "url": "<Bank API Url>",
      "secretUserName": "<Your Username Secret Key>",
      "secretPassword": "<Your Password Secret Key>",
      "secretClientAppIdent": "<Your Client Authentication>",
      "secretOrganizationId": "<Your Organization Id>"
    }
  }
}
```

**Response Codes:**

* `200`: Successful update.
* `401`: Authentication error.
* `404`: Bank ID not found.
* `500`: Server error.

## /member/

A Member is the representation of a person or entity on the Xand network. They are identified by their `address`. They have an `account`, which is associated with a `bank` and holds fiat currency. They also hold claims on the Xand network, which may be funded from their `account`, which may be sent to other Members, and which may be redeemed to their `account`.  Member API commands refer to the currently authenticated user on the Xand network.

* `GET /member/address`: Looks up the authenticated user's public address.
* `GET /member/balance`: Looks up the authenticated user's claims balance.

### GET /member/address

**Purpose:** Reveal the public identifier for the active Member's account, which is used to send them money.

**Response:** Returns a JSON object containing an `address`.
```
{
  "address": "5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz"
}
```

**Response Codes:**

* `200`: Successful retrieval.
* `401`: Authentication error.
* `500`: Server error.

### GET /member/balance

**Purpose:** Retrieve the balance for the active Member's account.

**Response:** Returns a JSON object with one key-pair, the balance in minor units (cents for USD).

_Example:_
```
{
  "balanceInMinorUnit": 0
}
```
* `balanceInMinorUnit`: Claims balance in a minor unit such as cents (for USD).

**Response Codes:**

* `200`: Successful retrieval.
* `401`: Authentication error.
* `500`: Server error.

## /service/

The service is the Xand network.

* `GET /service/health`: Enumerate the status for the Xand Network.

### GET /service/health

**Purpose:** Assess the health of the Xand Network.

**Response:** Returns a JSON object contains `up` and `down` arrays, each of which lists the status of various Xand-network services.
```
{
  "up": [
    {
      "service": "Xand API",
      "status": "up"
    },
    {
      "service": "Banks storage backend",
      "status": "up"
    },
    {
      "service": "Accounts storage backend",
      "status": "up"
    },
    {
      "service": "secret store",
      "status": "up"
    },
    {
      "service": "test-bank-one",
      "status": "up"
    },
    {
      "service": "test-bank-two",
      "status": "up"
    }
  ],
  "down": []
}
```
The above shows a fully functional Xand-network system, with all services functional.

## /transactions/

Transactions are the heart of the Xand network, allowing for the creation (`create`/`fund`), transfer (`send`), and redemption (`redeem`) of claims.

* `GET /transactions`: Look up a list of transactions for the active Member.
* `GET /transactions/{transactionId}`: Look up a specific transaction.
* `POST /transactions/claims/create`: Begin the process of creating new claims.
* `POST /transactions/claims/redeem`: Redeem claims to an account.
* `POST /transactions/claims/send`: Transfer claims to another member.
* `POST /transactions/claims/{transactionId}/fund`: Fund a newly created claim from an account.

### GET /transactions

**Purpose:** Retrieve all of a Member's transactions of all sorts.

**Query Parameters:**

* `pageSize`: Number of transactions to return per page [default: 10].
* `pageNumber`: Page of transactions to return [default: 0].

_Example:_
```
/api/v1/transactions?pageSize=10&pageNumber=0
```

**Response:** Returns a JSON object with a count of transactions and a JSON array of those transactions.  

_Example:_
```
{
  "total": 1,
  "transactions": [
    {
      "transactionId": "0xa138ea5b6f990b11906479b22e1b077bfbd35453be1b6106c1ffc0e8cdd812e8",
      "operation": "creationRequest",
      "signerAddress": "5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz",
      "amountInMinorUnit": 100000,
      "destinationAddress": "5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz",
      "nonce": "0x9169995b8c132c96e7e94c337fc01bf0",
      "bankAccount": {
        "id": 1,
        "bankId": 1,
        "shortName": "Account 0",
        "bankName": "test-bank-one",
        "maskedAccountNumber": "XXXXXXXX0000",
        "routingNumber": "990000000"
      },
      "status": {
        "state": "pending"
      },
      "datetime": "2022-03-14T18:15:18Z"
    }
  ]
}
```
* `total`: Number of transactions.
* `transactionId`: Identifier for a transaction.
* `operation`: Type of transaction, which is `creationRequest`, `payment`, or `redeem`.
* `signerAddress`: Public address of sender.
* `destinationAddress`: Public address of recipient (not included for redemptions).
* `amountInMinorUnit`: Amount of funds in transaction, where minor unit is cents for USD.
* `nonce` - Correlation ID used by Member and Trustee to link transactions to activities at deposit accounts (not included for transfers).
* `bankAccount` - Information on bank account for a creation or redemption request (not included for transfers).
* `state` - Current status of transaction, which is `cancelled`, `confirmed`, `invalid`, or `pending`.
* `datetime` - Time of transaction in Zulu time (GMT/UTC).

**Response Codes:**

* `200`: Successful retrieval.
* `401`: Authentication error.
* `500`: Server error.

### GET /transactions/{transactionId}

**Purpose:** Look up information on a specific transaction.

**Response:** Returns a JSON object containing one transaction, exactly as would be returned by `GET /transactions`.

_Example:_
```
{
  "transactionId": "0xa138ea5b6f990b11906479b22e1b077bfbd35453be1b6106c1ffc0e8cdd812e8",
  "operation": "creationRequest",
  "signerAddress": "5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz",
  "amountInMinorUnit": 100000,
  "destinationAddress": "5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz",
  "nonce": "0x9169995b8c132c96e7e94c337fc01bf0",
  "bankAccount": {
    "id": 1,
    "bankId": 1,
    "shortName": "Account 0",
    "bankName": "test-bank-one",
    "maskedAccountNumber": "XXXXXXXX0000",
    "routingNumber": "990000000"
  },
  "status": {
    "state": "pending"
  },
  "datetime": "2022-03-14T18:15:18Z"
}
```
* `transactionId`: Identifier for a transaction.
* `operation`: Type of transaction, `creationRequest`, `payment`, or `redeem`.
* `signerAddress`: Public address of sender.
* `destinationAddress`: Public address of recipient (not included for redemptions).
* `amountInMinorUnit`: Amount of funds in transaction, where minor unit is cents for USD.
* `nonce` - Correlation ID used by Member and Trustee to link transactions to activities at deposit accounts (not included for transfers).
* `bankAccount` - Information on bank account for a creation or redemption request (not included for transfers).
* `state` - Current status of transaction, which is `cancelled`, `confirmed`, `invalid`, or `pending`.
* `datetime` - Time of transaction in Zulu time (GMT/UTC).


**Response Codes:**

* `200`: Successful retrieval.
* `401`: Authentication error.
* `404`: Transaction ID not found.
* `500`: Server error, including incorrectly formatted Transaction ID.

### POST /transactions/claims/create

**Purpose:** Begin the process of claim creation for a Member.

**Request Body:** Requires a JSON object containing the `accountId` that will fund the Member and the amount of funds.

_Example:_
```
{
  "accountId": 1,
  "amountInMinorUnit": 100000
}
```
* `accountId`: Identifier for the account providing funds.
* `amountInMinorUnit` :- Amount of funds being send, where minor unit is cents for USD.

**Response:** Returns a JSON object containing the identifiers for the funding transaction.

_Example:_
```
{
  "transactionId": "0xa138ea5b6f990b11906479b22e1b077bfbd35453be1b6106c1ffc0e8cdd812e8",
  "nonce": "0x9169995b8c132c96e7e94c337fc01bf0"
}
```
* `transactionId`: Identifier for this transaction.
* `nonce`: Correlation ID used by Member and Trustee to link transactions to activities at deposit accounts.

**Response Codes:**

* `200`: Successful creation.
* `400`: Input or other client error.
* `401`: Authentication error.
* `500`: Server error including Bank ID not found.

> **Cryptographic Notes:** The identity of the Member creating claims, their ABA routing number, and their bank account number are all masked to everyone but the Trustee and the Member. 

> **Financial Notes:** This request creates a record for a new claim and provides the Member with a `nonce` to correlate the claim to a bank transfer. It is just the first part of a three-part process to transform fiat currency into claims on the Xand network:
> 
> 1. Create claims with `POST /transactions/claims/create`.
> 2. Fund claims with `POST /transactions/claims/{transactionId}/fund`.
> 3. Wait for Trustee to confirm funding.
> 
> The steps are all connected by inclusion of the same `nonce` as a correlation ID.

### POST /transactions/claims/redeem

**Purpose:** Transfer claims back into fiat currency at the Member's bank account.

**Request Body:** Requires a JSON object containing the account that funds are being sent to and the amount of money being redeemed.

_Example:_
```
{
  "accountId": 1,
  "amountInMinorUnit": 9999
}
```
* `accountId`: The account receiving the redemption. This must be the account associated with the active Member, else funds will become inaccessible.
* `amountInMinorUnit`: The amount of the redemption, where minor unit is cents for USD.

**Response:** Returns a JSON object containing a transaction ID and a nonce.
```
{
  "transactionId": "0x1749f72ce95e18c9eae8c28c0a35aae9915effa2168f3bdb3e5312a368bf4ed4",
  "nonce": "0xe2e03b07c98c9489855bd63d5de5a773"
}
```
The `nonce` may be used to correlate the transfer of funds into the Member's deposit account.

**Response Codes:**

* `200`: Successful creation.
* `400`: Input or other client error, including Account ID not found.
* `401`: Authentication error.
* `500`: Server error including Bank ID not found.

> **Cryptographic Notes:** The identity of the Member redeeming claims, their ABA routing number, and their bank account number are all masked to everyone but the Trustee and the Member. 

### POST /transactions/claims/send

**Purpose:** Sends claims from one Member to another.

**Request Body:** Requires a JSON object containing the recipient's address and the amount to transfer.

_Example:_
```
{
  "toAddress": "5GedXksPiCaXqv114r2NPQQbgirgA9X8pyRM1AHZ42bDGomj",
  "amountInMinorUnit": 233
}
```
* `toAddress`: The public address of the recipient, retrieved by them using `GET /member/address`.
* `amountInMinorUnit`: Amount of funds being send, where minor unit is cents for USD.

**Response:** Returns a JSON object contain the transaction identifier and a nonce.

_Example:_
```
{
  "transactionId": "0x457f6de36df3928ab192539f501762d84e4e6995cb10162d89df5c5629e029af",
  "nonce": null
}
```
Note that the `nonce` is currently empty because there is no need to correlate a `claims/send` transaction to a bank transaction.

**Response Codes:**

* `200`: Successful transfer.
* `400`: Input or other client error.
* `401`: Authentication error.
* `500`: Server error.

> **Cryptographic Notes:** The sending Member's identity, the receiving Member's identity, and the transaction amount are all hidden from everyone but the Members involved in the transaction.

> **Financial Notes:** This is the main function of the Xand network. It's how Members transfer funds (Claims) to each other, enabling real-time payment.

### POST /transactions/claims/{transactionId}/fund

**Purpose:** Fund a claim that has previously been created by transferring money from the Member's deposit account to the Trustee's deposit account at a bank.

**Request Body:** Requires a JSON object containing the transferring amount, the nonce for the transaction, and the bank being used.

_Example:_
```
{
  "amountInMinorUnit": 100000,
  "nonce": "0x9169995b8c132c96e7e94c337fc01bf0",
  "bankAccountId": 1
}
```
* `amountInMinorUnit`: Amount of funds being sent, where minor unit is cents for USD.
* `none`: Correlation ID from `/create` command.
* `bankAccountId`: Identifier for the Member's account.

The `nonce` and `amountInMinorUnit` both must match the claims creation transaction and the `bankAccountId` must match the `accountId` associated with the member. Incorrectly inputting the transaction information may transfer funds to the Trustee but will not close the claim creation. Incorrectly inputting the `bankAccountId` will result in nothing occurring.

**Response:** Returns a JSON object reiterating the amount transferred.

_Example:_
```
{
  "amountInMinorUnit": 100000
}
```

**Response Codes:**

* `200`: Successful funding.
* `400`: Input or other client error.
* `401`: Authentication error.
* `500`: Server error.

> **Cryptographic Notes:** The identity of the Member creating claims, their ABA routing number, and their bank account number are all masked to everyone but the Trustee and the Member. 

> **Financial Notes:** This request transfers funds from the Member's deposit account to the Trustee's deposit account at the same bank. It is the second part of a three-part process to transform fiat currency into claims on the Xand network:
> 
> 1. Create claims with `POST /transactions/claims/create`.
> 2. Fund claims with `POST /transactions/claims/{transactionId}/fund`.
> 3. Wait for Trustee to confirm funding.


