# Implement Page

Xand allows for the rapid, [trustless transfer](PLEASE-LINK-TO-PARAGRAPH-IN-HOW-IT-WORKS-WITH-DEFINITION-OF-TRUSTLESS) of funds between members using the following general workflow:

* **Configure.** Bank deposit accounts are registered with the Xand network.
* **Create.** Claims are created to fund Members on the Xand network, using bank deposit accounts.
* **Send** Claims are transferred among Members on the Xand network to make payments.
* **Redeem.** Claims are redeemed to return funds to bank deposit accounts from the Xand network.

Xand is accessed through a RESTful API. The Rest endpoint for Member API commands is `/api/{version}/` on the machine and port where Xand is running.

The standard REST architecture is used, with resources accessible using the GET, POST, PUT, and DELETE HTTP request methods. When additional data must be sent to Xand, it's done using the request body of a POST or PUT command. 
A full message for the simple `/member/address` GET command, accessed through curl, would be formatted as follows:
```
curl -X GET "http://localhost:3000/api/v1/member/address" -H  "accept: application/json" -H  "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJYQU5EX05FVFdPUktfR0VORVJBVE9SIiwic3ViIjoiREVWRUxPUEVSIiwiZXhwIjo0MTAyMzU4NDAwLCJjb21wYW55IjoiQVdFU09NRUNPIn0.wMe5Ji5CKhK5xcOP9RR0mIYUGoB8HVs6NZbN0PtKeIM"
```

The API endpoints for the Xand Member API are currently divided into five categories:

* `/accounts/` - View participant accounts.
* `/banks/` - View participating banks.
* `/member/` - View current user.
* `/service/` -  Examine overall service.
* `/transactions/` - Create, transfer, or redeem claims.

