# Implement: Send

You can receive claims on your Member address, either because you followed the Create workflow or because other Members sent you claims. Once you have claims, you can send them to other Members of the Xand Network. This will allow you to make payments to those Members using the fast, final transactions of the Xand Network. All that is required to initiate a transaction is the address of the Member that you're sending to. 

This is a one-step process:

* `POST /transactions/claims/send` — Sends claims from one Member to another.

***Cryptographic Protections:*** When using the system, all transaction information, including the identity of the sender, the identity of the recipient, and the transaction amount, is masked, except to the sender, the recipient, and the Trustee.

## POST /transactions/claims/send
#### Sends claims from one Member to another.

To send a transaction to another Member, first you must know their address. Communication of this address should usually be managed in some secure out-of-band way, such as the other Member sending it over a secure-messaging system, and then verifying over voice or video. Once you have a destination address, you can use the `POST /transactions/claims/send` command to send a chosen amount of claims to the address. 

After a transaction has been sent, settlement will be complete in less than 60 seconds. You may see the state of the transaction with the `GET /transactions` or `GET /transactions/{transactionid}` commands and after its `state` has moved from `pending` to `confirmed`, the recipient will see it added to their balance with the `GET /member/balance` command.

### Example Request

This sends 233 units, which is $2.33 if the unit is USD, to the Member identified by the address `5GedXksPiCaXqv114r2NPQQbgirgA9X8pyRM1AHZ42bDGomj`.

```
{
  "toAddress": "5GedXksPiCaXqv114r2NPQQbgirgA9X8pyRM1AHZ42bDGomj",
  "amountInMinorUnit": 233
}
```

### Request Fields

#### toAddress
[required, string]

The public address of the recipient.
> Note: See`GET /member/address` for own address retrieval instructions. 

#### amountInMinorUnit
[required, int]

Amount of funds being sent, where minor unit is cents for USD.

### Example Response

```
{
  "transactionId": "0x457f6de36df3928ab192539f501762d84e4e6995cb10162d89df5c5629e029af",
  "nonce": null
}
```

### Response Fields

#### transactionId
[string]

The identifier for the transaction, which can be referenced with the `GET /transactions` or `GET /transactions/{transactionid}` commands.

#### nonce
[string]

Empty Field.

Note: `nonce` is a correlation ID used as part of the Create or Redeem workflows. It will be empty when you utilize the Send workflow described here.
