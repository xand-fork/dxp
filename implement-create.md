# Implement: Create

Once you have configured your bank information and retrieved your Member address, you can create claims on the Xand network, which is what makes money in your bank deposit account available for sending on the network. The create process works by transferring money from your bank deposit account to a Trustee account at your bank (also known as "funding" your create request). Once that occurs, a corresponding claim is then created on the Xand network, which allows you to transfer up to that value to other Members.

This is a two-step process:

* `POST /transactions/claims/create` - Initiate an creation claim for a Member.
* `POST /transactions/claims/{transactionId}/fund` - Fund an existing creation claim.

The Claim workflow may be used whenever you want to add funds to your Member account.

## POST /transactions/claims/create
#### Initiate an creation claim for a Member.

To begin the process of creating claims on the Xand Network, use `POST /transactions/claims/create`, which will initiate a claim-creation request that will be valid for 24 hours. You must supply a bank `accountId` that you retrieved during the Configuration step, and an amount. You will receive a response with a `transactionId` and a `nonce`, which you use to fund your claim in the next step, **required to complete your claim creation**.
>Note: The amount will be transferred from your bank deposit account to the Xand Network in the funding step

### Example Request

This indicates an eventual creation of 100,000 units on the Member's ledger on the Xand network, which is $1,000.00 if the unit is USD, from bank `accountID` `1` in the system.

```
{
  "accountId": 1,
  "amountInMinorUnit": 100000
}
```

### Request Fields

#### accountId
[required, integer]

Identifier for the bank deposit account providing funds, can be retrieved from `POST /accounts`

#### amountInMinorUnit
[required, integer]

Amount of funds that will be transferred into a claim, where minor unit is cents for USD.

### Example Response

{
  "transactionId": "0xa138ea5b6f990b11906479b22e1b077bfbd35453be1b6106c1ffc0e8cdd812e8",
  "nonce": "0x9169995b8c132c96e7e94c337fc01bf0"
}

### Response Fields

#### transactionId
[string (hex)]

Identifier for this transaction. To be used in the creation funding step. 

#### nonce
[string]

Correlation ID. To be used in the creation funding step.

## POST /transactions/claims/{transactionId}/fund
#### Fund an existing creation claim.

To complete the process of creating claims on the Xand network, use `POST /transactions/claims/{transactionId}/fund`, where `{transactionId}` was retrieved in the response body from the `POST /transactions/claims/create` call that initiated this process. This will transfer the funds from your bank deposit account to the Trustee account, and after the Trustee has verified the transfer, will then create a corresponding claim on the Xand network. You may then Send claims to other members and also may Redeem claims back into your bank account as desired.

### Example Request

This API call funds the $1,000 USD claim creation initiated above, and in this case is sent as a request to the endpoint `/transactions/claims/0xa138ea5b6f990b11906479b22e1b077bfbd35453be1b6106c1ffc0e8cdd812e8/fund`, where the `transactionId` is `0xa138ea5b6f990b11906479b22e1b077bfbd35453be1b6106c1ffc0e8cdd812e8`.

```
{
  "amountInMinorUnit": 100000,
  "nonce": "0x9169995b8c132c96e7e94c337fc01bf0",
  "bankAccountId": 1
}
```

### Request Fields

#### amountInMinorUnit
[required, integer]

Amount of funds being utilized to fund the create claim, where minor unit is cents for USD. This must match the `amountInMinorUnit` sent with the `POST /transactions/claims/create` request.

#### nonce
[required, string]

Correlation ID. This **must** match the `nonce` retrieved from the `POST /transactions/claims/create` response. Incorrect entry of the `nonce` may result in funds being transferred out of your bank deposit account without claims being created.

#### bankAccountId
[required, integer]

Identifier for the Member's registered bank account. This must match the `accountId` sent with the `POST /transactions/claims/create` request. Incorrect entry of the `bankAccountId` will likely result in nothing occurring.

### Example Response
```
{
  "amountInMinorUnit": 100000
}
```

### Response Fields

### amountInMinorUnit
[required, integer]

Amount of funds being used to fund the create claim, where minor unit is cents for USD.

